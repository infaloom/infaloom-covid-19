﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfaloomCovid.Web.DTO
{
    public class DataSourceDTO
    {
        public DataSourceRecordDTO[] records { get; set; }
    }
}
