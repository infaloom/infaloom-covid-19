﻿using InfaloomCovid.Web.Helpers;
using Newtonsoft.Json;
using System;

namespace InfaloomCovid.Web.DTO
{
    public class DataSourceRecordDTO
    {
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime dateRep { get; set; }
        public string day { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public int cases { get; set; }
        public int deaths { get; set; }
        public string countriesAndTerritories { get; set; }
        public string geoId { get; set; }
        public string countryterritoryCode { get; set; }
        public int? popData2019 { get; set; }
        public string continentExp { get; set; }
        [JsonProperty("Cumulative_number_for_14_days_of_COVID-19_cases_per_100000")]
        public decimal? Cumulative_number_for_14_days_of_COVID19_cases_per_100000 { get; set; }
    }
}
