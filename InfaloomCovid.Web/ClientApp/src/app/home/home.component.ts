import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { HttpClient } from '@angular/common/http';

import * as moment from 'moment';

interface RecordDTO {
  dateRep: string;
  cases: number;
  cumulative_number_for_14_days_of_COVID19_cases_per_100000: number;
}

interface StatsDTO {
  records: RecordDTO[];
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  cases: number[];
  avgNew: number[];
  threshold: number[];

  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor = 'chart'; // optional string, defaults to 'chart'
  chartOptions: Highcharts.Options = {
    title: null,
    tooltip: {
      shared: true,
    },
    yAxis: {
      title: null,
    },
  }; // Highcharts.defaultOptions; // required
  updateFlag = false; // optional boolean
  oneToOneFlag = true; // optional boolean, defaults to false
  runOutsideAngular = false; // optional boolean, defaults to false
  chartCallback: Highcharts.ChartCallbackFunction = function (chart) {}; // optional function, defaults to null

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.http.get<StatsDTO>('/api/stats').subscribe((d) => {
      const dates = d.records.map((r) => moment.utc(r.dateRep).format('DD.MM.YYYY.'));
      this.chartOptions = {
        ...this.chartOptions,
        ...{
          xAxis: {
            type: 'datetime',
            categories: dates,
          },
        },
      };

      this.cases = d.records.map((r) => r.cases);
      this.avgNew = d.records.map(
        (r) => r.cumulative_number_for_14_days_of_COVID19_cases_per_100000
      );
      this.threshold = d.records.map((r) => 20);
      this.chartOptions = {
        ...this.chartOptions,
        ...{
          series: [
            {
              color: 'blue',
              name: 'Novozaraženi',
              data: this.cases,
              type: 'line',
            },
            {
              color: 'red',
              name:
                'Prosek novozareženih na 100k stanovnika u poslednjih 14 dana',
              data: this.avgNew,
              type: 'line',
            },
            {
              color: 'green',
              dashStyle: 'Dash',
              name: 'Granica za povratak u office',
              data: this.threshold,
              type: 'line',
            },
          ],
        },
      };
    });
  }

  lastDayCases() {
    return this.cases && this.cases.length ? this.cases[this.cases.length - 1] : null;
  }

  lastAvg() {
    return this.avgNew && this.avgNew.length ? this.avgNew[this.avgNew.length - 1] : null;
  }
}
