﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InfaloomCovid.Web.DTO;
using InfaloomCovid.Web.Jobs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace InfaloomCovid.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        private readonly IMemoryCache _memoryCache;
        private readonly JobsService _jobsService;

        public StatsController(IMemoryCache memoryCache, JobsService jobsService)
        {
            _memoryCache = memoryCache;
            _jobsService = jobsService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetStats(string country = "Serbia")
        {
            string json;
            if (!_memoryCache.TryGetValue("data", out json))
            {
                await _jobsService.CacheData();
                json = _memoryCache.Get<string>("data");
            }
            var stats = new StatsDTO()
            {
                records = JsonConvert.DeserializeObject<DataSourceDTO>(json).records.Where(r => r.countriesAndTerritories.Equals(country)).OrderBy(r => r.dateRep).ToArray()
            };
            return Ok(stats);
        }
    }
}