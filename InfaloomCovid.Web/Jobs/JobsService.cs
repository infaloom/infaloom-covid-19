﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InfaloomCovid.Web.Jobs
{
    public class JobsService
    {
        private readonly IMemoryCache _memoryCache;

        public JobsService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public async Task CacheData()
        {
            using (var wc = new WebClient())
            {
                var json = wc.DownloadString("https://opendata.ecdc.europa.eu/covid19/casedistribution/json/");
                _memoryCache.Set("data", json, TimeSpan.FromHours(1));
            }
        }
    }

}
